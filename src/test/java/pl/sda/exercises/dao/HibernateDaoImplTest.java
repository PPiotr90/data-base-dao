package pl.sda.exercises.dao;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sda.exercises.*;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class HibernateDaoImplTest {

    Utilities utilities = new Utilities();
    HibernateDaoImpl hibernateDao = new HibernateDaoImpl();
    List<Person> allPersonTemp = new ArrayList<>();
    List<City> allCityTemp = new ArrayList<>();
    List<Hobby> allHobbyTemp = new ArrayList<>();
    List<Profession> allProfessionTemp = new ArrayList<>();

    @BeforeEach
    public void beforeTests() {
        try {
           allPersonTemp  = hibernateDao.getAllPerson();
           allCityTemp  = hibernateDao.getAllCity();
            allHobbyTemp = hibernateDao.getAllHobby();
            allProfessionTemp = hibernateDao.getAllProfession();
            hibernateDao.getAllPerson().stream().forEach(person -> hibernateDao.deletePerson(person));
            hibernateDao.getAllCity().forEach(city -> hibernateDao.deleteCity(city));
            hibernateDao.getAllProfession().forEach(profession -> hibernateDao.deleteProfession(profession));
            hibernateDao.getAllHobby().forEach(hobby -> hibernateDao.deleteHobby(hobby));

        } catch ( Exception e ) {
            System.out.println(e.getMessage());
        }

    }
    @AfterEach
    public  void afterTests() {
        try{
            hibernateDao.getAllPerson().forEach(person -> hibernateDao.deletePerson(person));
        hibernateDao.getAllCity().forEach(city -> hibernateDao.deleteCity(city));
        hibernateDao.getAllHobby().forEach(hobby -> hibernateDao.deleteHobby(hobby));
        hibernateDao.getAllProfession().forEach(profession -> hibernateDao.deleteProfession(profession));
        hibernateDao.addAllPeople(allPersonTemp);
        hibernateDao.addAllCities(allCityTemp);
        hibernateDao.addAllHobbies(allHobbyTemp);
        hibernateDao.addAllProfession(allProfessionTemp);


        } catch ( Exception e ) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void addPerson() {

        Person person = new Person();
        person.setName("kogut");
        person.setSurname("a12");
        int id =hibernateDao.addPerson(person);
        Person personFromDB = hibernateDao.findPersonById(id);

        assertThat(personFromDB.getName()).isEqualTo("kogut");
        assertThat(personFromDB.getSurname()).isEqualTo("a12");


    }

    @Test
    void addCity() {
        City city = new City();
        city.setName("J-city");
        int id = hibernateDao.addCity(city);
        City cityFromDB = hibernateDao.findCityById(id);

        assertThat(city.getName()).isEqualTo(cityFromDB.getName());
    }

    @Test
    void addProfession() {
        Profession profession = new Profession();
        profession.setName("Actor");
        profession.setSalary(5000);
        int id = hibernateDao.addProfession(profession);
        Profession professionFromDB = hibernateDao.findProfessionById(id);

        assertThat(profession.getName()).isEqualTo(professionFromDB.getName());
    }

    @Test
    void addHobby() {

        Hobby hobby = new Hobby();
        hobby.setName("Sailing");
        int id = hibernateDao.addHobby(hobby);
        Hobby hobbyFromDB = hibernateDao.findHobbyById(id);

        assertThat(hobby.getName()).isEqualTo(hobbyFromDB.getName());
    }


    @Test
    void getAllPerson() {
        Person person = new Person();
        Person person1 = new Person();
        Person person2 = new Person();
        Person person3 = new Person();
        Person person4 = new Person();
        person.setName("a");
        person.setSurname("a");
        person1.setSurname("g");
        person1.setName("g");
        person3.setName("l");
        person3.setSurname("l");
        person2.setSurname("123");
        person2.setName("123");
        person4.setName("aa");
        person4.setSurname("aa");
        List<Person> people = new ArrayList<>();
        people.add(person);
        people.add(person1);
        people.add(person2);
        people.add(person3);
        people.add(person4);
        people.stream()
                .forEach(person5 -> hibernateDao.addPerson(person5));

       List<Person> peopleFromDB = hibernateDao.getAllPerson();
        assertThat(peopleFromDB).isNotEmpty();
       assertThat(person).isEqualTo(peopleFromDB.get(0));
       assertThat(person2).isEqualTo(peopleFromDB.get(2));
       assertThat(person4).isEqualTo(peopleFromDB.get(4));

    }



    @Test
    void deleteCity() {

        City city = new City();
        city.setName("Komuna");
        int id =hibernateDao.addCity(city);
        assertThat(hibernateDao.findCityById(id)).isEqualTo(city);

hibernateDao.deleteCity(city);
assertThat(hibernateDao.findCityById(id)).isNull();
    }



    @Test
    void deleteAllPerson() {
        Person person = new Person();
        Person person1 = new Person();
        Person person2 = new Person();
        Person person3 = new Person();
        Person person4 = new Person();
        person.setName("a");
        person.setSurname("a");
        person1.setSurname("g");
        person1.setName("g");
        person3.setName("l");
        person3.setSurname("l");
        person2.setSurname("123");
        person2.setName("123");
        person4.setName("aa");
        person4.setSurname("aa");
        List<Person> people = new ArrayList<>();
        people.add(person);
        people.add(person1);
        people.add(person2);
        people.add(person3);
        people.add(person4);
        people.stream()
                .forEach(person5 -> hibernateDao.addPerson(person5));

        hibernateDao.deleteAllPerson();

        assertThat(hibernateDao.getAllPerson()).isEmpty();
    }




    @Test
    void updateHobby() {

        Hobby hobby = new Hobby();
        hobby.setName("Sailing");
        final int id = hibernateDao.addHobby(hobby);
        Hobby hobbyNew = new Hobby();
        hobbyNew.setName("Skiing");
        hobbyNew.setId(id);
        hibernateDao.updateHobby(hobbyNew);

        assertThat(hobbyNew.getName()).isEqualTo(hibernateDao.findHobbyById(id).getName());
        assertThat(hobbyNew.getId()).isEqualTo(hibernateDao.findHobbyById(id).getId());
    }
}