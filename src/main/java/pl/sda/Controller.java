package pl.sda;


import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import pl.sda.exercises.City;
import pl.sda.exercises.Hobby;
import pl.sda.exercises.Person;
import pl.sda.exercises.Profession;
import pl.sda.exercises.dao.HibernateDaoImpl;

import java.util.ArrayList;
import java.util.List;

public class Controller {
    @FXML
    ListView<Person> personListView;
    @FXML
    ListView<City> cityListView;
    @FXML
    ListView<Profession> professionListView;
    @FXML
    ListView<Hobby> hobbyListView;
    @FXML
    TextField textFieldCity;
    @FXML
    TextField textFieldProfession;
    @FXML
    TextField textFieldHobbies;
    @FXML
    TextField textFieldPersonName;
    @FXML
    TextField textFieldPersonSurname;
    @FXML
    TextField textFieldCityName;
    @FXML
    TextField textFieldHobbyName;
    @FXML
    TextField textFieldProfessionName;
    @FXML
    TextField textFieldProfessionSalary;
    @FXML
    Button addPersonButton;
    @FXML
    Button addCityButton;
    @FXML
    Button addHobbyButton;
    @FXML
    Button addProfessionButton;
    @FXML
    Button createProfessionButton;
    @FXML
    Button createPersonButton;
    @FXML
    Button createHobbyButton;
    @FXML
    Button createCityButton;
    @FXML
    Button changeCityButton;
    @FXML
    Button changeProfessionButton;
    @FXML
    Button changeHobbyButton;
    @FXML
    Button clearButton;
    @FXML
    Button deletePersonButton;
    @FXML
    Button deleteCityButton;
    @FXML
    Button deleteHobbyButton;
    @FXML
    Button deleteProfessionButton;


    HibernateDaoImpl dao = new HibernateDaoImpl();
    List<Person> allPerson;
    List<City> allCity;
    List<Profession> allProfession;
    List<Hobby> allHobby;

    Person personFromList;
    City cityFromList;
    Hobby hobbyFromList;
    Profession professionFromList;



    public void initialize() {
        textFieldCity.setEditable(false);
        textFieldProfession.setEditable(false);
        textFieldHobbies.setEditable(false);
        setVisibilityChangeButton(false);
        deleteCityButton.setVisible(false);
        deleteHobbyButton.setVisible(false);
        deletePersonButton.setVisible(false);
        deleteProfessionButton.setVisible(false);


        personListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                personFromList = personListView.getSelectionModel().getSelectedItem();
                textFieldPersonName.setText(personFromList.getName());
                textFieldPersonSurname.setText(personFromList.getSurname());
                if (personFromList.getProfession() != null)
                    textFieldProfession.setText(personFromList.getProfession().getName());
                if (personFromList.getCity() != null) textFieldCity.setText(personFromList.getCity().getName());
                textFieldHobbies.setText(personFromList.hobbiesPresentation());
                createPersonButton.setText("Save Person");
                setVisibilityChangeButton(true);
                if (hobbyFromList != null) {
                    if (personFromList.getHobbies().contains(hobbyFromList)) {
                        changeHobbyButton.setText("Remove hobby");
                    } else changeHobbyButton.setText("Add Hobby");
                }
                deletePersonButton.setVisible(true);

            }
        });

        cityListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                cityFromList = cityListView.getSelectionModel().getSelectedItem();
                textFieldCityName.setText(cityFromList.getName());
                createCityButton.setText("Save City");
                changeCityButton.setText(" Change City");
                deleteCityButton.setVisible(true);
            }
        });
        hobbyListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                hobbyFromList = hobbyListView.getSelectionModel().getSelectedItem();
                textFieldHobbyName.setText(hobbyFromList.getName());
                createHobbyButton.setText("Save Hobby");
                if (personFromList != null) {
                    if (personFromList.getHobbies().contains(hobbyFromList)) {
                        changeHobbyButton.setText("Remove hobby");
                    } else changeHobbyButton.setText("Add Hobby");
                }
                deleteHobbyButton.setVisible(true);
            }
        });
        professionListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                professionFromList = professionListView.getSelectionModel().getSelectedItem();
                textFieldProfessionName.setText(professionFromList.getName());
                textFieldProfessionSalary.setText(String.valueOf(professionFromList.getSalary()));
                createProfessionButton.setText("Save Profession");
                changeProfessionButton.setText("Change Profession");
                deleteProfessionButton.setVisible(true);
            }
        });

        updateLists();

        addPersonButton.setOnAction(event -> {
            personListView.getSelectionModel().clearSelection();
            textFieldPersonName.setText("");
            textFieldPersonSurname.setText("");
            textFieldCity.setText("");
            textFieldHobbies.setText("");
            textFieldProfession.setText("");
            createPersonButton.setText("Create Person");
            personFromList = null;
            setVisibilityChangeButton(false);
            deletePersonButton.setVisible(false);
        });
        addCityButton.setOnAction(event -> {
            cityListView.getSelectionModel().clearSelection();
            textFieldCityName.setText("");
            createCityButton.setText("Create City");
            cityFromList = null;
            deleteCityButton.setVisible(false);
        });

        addHobbyButton.setOnAction(event -> {
            hobbyListView.getSelectionModel().clearSelection();
            textFieldHobbyName.setText("");
            createHobbyButton.setText("Create Hobby");
            hobbyFromList = null;
            deleteHobbyButton.setVisible(false);
        });
        addProfessionButton.setOnAction(event -> {
            professionListView.getSelectionModel().clearSelection();
            textFieldProfessionName.setText("");
            textFieldProfessionSalary.setText("");
            createProfessionButton.setText("Create Profession");
            professionFromList = null;
            deleteProfessionButton.setVisible(false);


        });

        createPersonButton.setOnAction(event -> {
            boolean personIsSelected = personFromList != null;
            Person newPerson = new Person();

            newPerson.setName(textFieldPersonName.getText());
            newPerson.setSurname(textFieldPersonSurname.getText());
            if (personIsSelected) {

                newPerson.setId(personFromList.getId());

                newPerson.setCity(personFromList.getCity());
                newPerson.setProfession(personFromList.getProfession());
                newPerson.setHobbies(personFromList.getHobbies());
                dao.updatePerson(newPerson);


            } else {

                if (hobbyFromList != null) newPerson.addHobby(hobbyFromList);
                if (cityFromList != null) newPerson.setCity(cityFromList);
                if (professionFromList != null) newPerson.setProfession(professionFromList);

                dao.addPerson(newPerson);


            }
            updateLists();
            personListView.getSelectionModel().select(newPerson);


        });

        createCityButton.setOnAction(event -> {
            boolean cityIsSelected = cityFromList != null;
            City newCity = new City();
            newCity.setName(textFieldCityName.getText());
            if (cityIsSelected) {
                newCity.setId(cityFromList.getId());
                dao.updateCity(newCity);
            } else dao.addCity(newCity);

            updateLists();
            cityListView.getSelectionModel().select(newCity);
        });

        createHobbyButton.setOnAction(event -> {
            Hobby newHobby = new Hobby();
            newHobby.setName(textFieldHobbyName.getText());
            if (hobbyFromList != null) {
                newHobby.setId(hobbyFromList.getId());
                dao.updateHobby(newHobby);

            } else dao.addHobby(newHobby);
            updateLists();
            hobbyListView.getSelectionModel().select(newHobby);
        });

        createProfessionButton.setOnAction(event -> {
            Profession newProfession = new Profession();
            newProfession.setName(textFieldProfessionName.getText());
            newProfession.setSalary(Integer.valueOf(textFieldProfessionSalary.getText()));
            if (professionFromList != null) {
                newProfession.setId(professionFromList.getId());
                dao.updateProfession(newProfession);

            } else dao.addProfession(newProfession);
            updateLists();
            professionListView.getSelectionModel().select(newProfession);
        });

        changeCityButton.setOnAction(event -> {
            personFromList.setCity(cityFromList);
            dao.updatePerson(personFromList);
            updateLists();

        });
        changeProfessionButton.setOnAction(event -> {
            personFromList.setProfession(professionFromList);
            dao.updatePerson(personFromList);
            updateLists();

        });
        changeHobbyButton.setOnAction(event -> {
            if (hobbyFromList == null) {
                personFromList.setHobbies(new ArrayList<Hobby>());
            } else {
                if (personFromList.getHobbies().contains(hobbyFromList)) {
                    personFromList.deleteHobby(hobbyFromList);
                } else personFromList.addHobby(hobbyFromList);
            }
            dao.updatePerson(personFromList);
            dao.updateHobby(hobbyFromList);

            updateLists();

        });

clearButton.setOnAction(event -> {
    personListView.getSelectionModel().clearSelection();
    cityListView.getSelectionModel().clearSelection();
    hobbyListView.getSelectionModel().clearSelection();
    professionListView.getSelectionModel().clearSelection();
    deleteCityButton.setVisible(false);
    deleteHobbyButton.setVisible(false);
    deletePersonButton.setVisible(false);
    deleteProfessionButton.setVisible(false);
});

deletePersonButton.setOnAction(event -> {dao.deletePerson(personFromList);
updateLists();});
deleteCityButton.setOnAction(event -> {dao.deleteCity(cityFromList);
updateLists();});
deleteHobbyButton.setOnAction(event -> {dao.deleteHobby(hobbyFromList);
updateLists();});
deleteProfessionButton.setOnAction(event -> {dao.deleteProfession(professionFromList);
updateLists();});
    }


    private void setLists() {
        personListView.getItems().setAll(allPerson);
        cityListView.getItems().setAll(allCity);
        professionListView.getItems().setAll(allProfession);
        hobbyListView.getItems().setAll(allHobby);
    }

    private void updateLists() {
        allPerson = dao.getAllPerson();
        allCity = dao.getAllCity();
        allProfession = dao.getAllProfession();
        allHobby = dao.getAllHobby();
        setLists();
    }

    private void setVisibilityChangeButton(boolean set) {
        changeCityButton.setVisible(set);
        changeProfessionButton.setVisible(set);
        changeHobbyButton.setVisible(set);
    }


}
