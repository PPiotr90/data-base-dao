package pl.sda.exercises;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Data
@Entity
@Table(name = "profession")
public class Profession implements Serializable {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private  int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private  int salary;
    @OneToMany(mappedBy = "profession", fetch =  FetchType.EAGER)
    private Set<Person> personSet = new HashSet<>();

    @Override
    public String toString() {
        return name +
                ", salary=" + salary;
    }
}
