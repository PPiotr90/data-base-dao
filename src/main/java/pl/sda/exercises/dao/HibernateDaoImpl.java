package pl.sda.exercises.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import pl.sda.exercises.City;
import pl.sda.exercises.Hobby;
import pl.sda.exercises.Person;
import pl.sda.exercises.Profession;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

public class HibernateDaoImpl implements DaoOperations {

StandardServiceRegistry registry =  new StandardServiceRegistryBuilder()
        .configure("hibernate.cfg.xml")
        .build();

Metadata metadata = new MetadataSources(registry)
        .buildMetadata();
SessionFactory sessionFactory = metadata.buildSessionFactory();

private void getTransaction(Consumer<Session> action) {
Session session = sessionFactory.openSession();
    final Transaction transaction = session.beginTransaction();
    try{
        action.accept(session);
        transaction.commit();

    } catch (Exception e) {
        transaction.rollback();
        System.out.println(e.getMessage());
    }

    session.close();
}



    @Override
    public int addPerson(Person person) {
    this.getTransaction(session -> session.save(person));
    return person.getId();

    }

    @Override
    public int addCity(City city) {
    getTransaction(session -> session.save(city));
    return  city.getId();

    }

    @Override
    public int addProfession(Profession profession) {
    getTransaction(session -> session.save(profession));
    return  profession.getId();
    }

    @Override
    public int addHobby(Hobby hobby) {
    getTransaction(session -> session.save(hobby));

    return  hobby.getId();

    }

    @Override
    public Person findPersonById(int id) {
    Person person = null;
  Session session = sessionFactory.openSession();
  Transaction transaction = session.beginTransaction();
  try{
      person = session.get(Person.class, id);


  } catch (Exception e) {
      transaction.rollback();
  }

  session.close();
  return  person;
    }

    @Override
    public City findCityById(int id) {
        City city = null;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{

            city=session.get(City.class, id);
            transaction.commit();
        } catch ( Exception e ) {
            transaction.rollback();
            System.out.println(e.getMessage());
        }
        session.close();
        return  city;
    }

    @Override
    public Profession findProfessionById(int id) {
        Profession profession = null;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{

            profession=session.get(Profession.class, id);
            transaction.commit();
        } catch ( Exception e ) {
            transaction.rollback();
            System.out.println(e.getMessage());
        }
        session.close();
        return  profession;
    }

    @Override
    public Hobby findHobbyById(int id) {
        Hobby hobby = null;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{

            hobby=session.get(Hobby.class, id);
            transaction.commit();
        } catch ( Exception e ) {
            transaction.rollback();
            System.out.println(e.getMessage());
        }
        session.close();
        return  hobby;
    }

    @Override
    public List<Person> getAllPerson() {
        Session session = sessionFactory.openSession();
        List<Person> people = session
                .createQuery("FROM Person", Person.class)
                .list();

        session.close();


        return  people;
    }

    @Override
    public List<City> getAllCity() {
        Session session = sessionFactory.openSession();
        List<City> cities = session
                .createQuery("From City", City.class)
                .list();

        return  cities;
    }

    @Override
    public List<Hobby> getAllHobby() {
        Session session = sessionFactory.openSession();
        List<Hobby> hobbies = session
                .createQuery("From Hobby", Hobby.class)
                .list();

        return  hobbies;
    }

    @Override
    public List<Profession> getAllProfession() {
        Session session = sessionFactory.openSession();
        List<Profession> professions = session
                .createQuery("From Profession", Profession.class)
                .list();

        return  professions;
    }

    @Override
    public void deletePerson(Person person) {
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();
        try {
            if(person.getHobbies().size()>0) {
                person.getHobbies().stream()
                        .forEach(hobby -> hobby.getPeople().remove(person));
                person.setHobbies(null);
            }
            if(person.getCity()!=null) {person.getCity().getPersonSet().remove(person);
            person.setCity(null);
            }
            if(person.getProfession()!=null) {person.getProfession().getPersonSet().remove(person);
            person.setProfession(null);}
            session.delete(person);

            transaction.commit();
                } catch (Exception e) {
                    transaction.rollback();
            System.out.println(e.getMessage());
                }
        session.close();

    }

    @Override
    public void deleteCity(City city) {
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();
        try {
            if(city.getPersonSet().size()>0) {
                city.getPersonSet().stream()
                        .forEach(person -> person.setCity(null));
            }
            city.setPersonSet(null);
            session.delete(city);

            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            System.out.println(e.getMessage());
        }
session.close();
    }

    @Override
    public void deleteProfession(Profession profession) {
    final Session session = sessionFactory.openSession();
    final Transaction transaction = session.beginTransaction();
        try {
        if(profession.getPersonSet().size()>0) {
            profession.getPersonSet().stream()
                    .forEach(person -> person.setProfession(null));
        }
        profession.setPersonSet(null);
        session.delete(profession);

        transaction.commit();
    } catch (Exception e) {
        transaction.rollback();
        System.out.println(e.getMessage());
    }
session.close();
}

    @Override
    public void deleteHobby(Hobby hobby) {
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();
        try {
            if(hobby.getPeople().size()>0) {
                hobby.getPeople().stream()
                        .forEach(person -> person.getHobbies().remove(hobby));

            }
            hobby.setPeople(null);
            session.delete(hobby);
            transaction.commit();

        } catch (Exception e) {
            transaction.rollback();
        }
session.close();
    }

    @Override
    public void deleteAllPerson() {
    getAllPerson().stream()
            .forEach(this::deletePerson);

    }

    @Override
    public void deleteAllCity() {
    getAllCity().stream()
            .forEach(this::deleteCity);

    }

    @Override
    public void deleteAllProfession() {
    getAllProfession().stream()
            .forEach(this::deleteProfession);

    }

    @Override
    public void deleteAllHobby() {
    getAllHobby().stream()
            .forEach(this::deleteHobby);

    }

    @Override
    public void updatePerson(Person person) {
        getTransaction(session -> session.update(person));
    }

    @Override
    public void updateCity(City city) {
        getTransaction(session -> session.update(city));

    }

    @Override
    public void updateProfession(Profession profession) {
        getTransaction(session -> session.update(profession));

    }

    @Override
    public void updateHobby(Hobby hobby) {
        getTransaction(session -> session.update(hobby));

    }

    @Override
    public void addAllPeople(Collection<Person> people) {
    people.stream()
            .forEach(this::addPerson);

    }

    @Override
    public void addAllCities(Collection<City> cities) {
 cities.stream().forEach(this::addCity);
    }

    @Override
    public void addAllHobbies(Collection<Hobby> hobbies) {
hobbies.stream().forEach(this::addHobby);
    }

    @Override
    public void addAllProfession(Collection<Profession> professions) {
    professions.forEach(this::addProfession);

    }

    public  void updateProfessionForPerson( int personId, int professionId) {
    Session session = sessionFactory.openSession();
    session.beginTransaction();
    try{
        Person person1 = this.findPersonById(personId);
        Profession profession1 = this.findProfessionById(professionId);
        person1.addProfession(profession1);


        session.getTransaction().commit();
    } catch ( Exception e) {
        session.getTransaction().rollback();
    }

    session.close();



    }

    public  void  updateCityForPerson( int personId, int cityId) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Person person1 = this.findPersonById(personId);
            City city = this.findCityById(cityId);
            person1.addCity(city);


            session.getTransaction().commit();
        } catch ( Exception e) {
            session.getTransaction().rollback();
        }

        session.close();



    }
    public  void deleteCityInPerson(int personId){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Person person1 = this.findPersonById(personId);
            person1.setCity(null);


            session.getTransaction().commit();
        } catch ( Exception e) {
            session.getTransaction().rollback();
        }

        session.close();



    }



    public  void  deleteProfessionInPerson(int personId){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Person person1 = this.findPersonById(personId);
            person1.setProfession(null);


            session.getTransaction().commit();
        } catch ( Exception e) {
            session.getTransaction().rollback();
        }

        session.close();



    }


    public  void  deleteHobbyInPerson(int personId, int hobbyId){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Person person1 = this.findPersonById(personId);
            Hobby hobby = this.findHobbyById(hobbyId);

            person1.getHobbies().remove(hobby);
            hobby.getPeople().remove(person1);


            session.getTransaction().commit();
        } catch ( Exception e) {
            session.getTransaction().rollback();
        }

        session.close();



    }
}
