package pl.sda.exercises.dao;

import pl.sda.exercises.City;
import pl.sda.exercises.Hobby;
import pl.sda.exercises.Person;
import pl.sda.exercises.Profession;

import java.util.Collection;
import java.util.List;

public interface DaoOperations {

    public int addPerson(Person person);

    public int addCity(City city);

    public int addProfession(Profession profession);

    public int addHobby(Hobby hobby);

    public Person findPersonById(int id);

    public City findCityById(int id);

    public Profession findProfessionById(int id);

    public Hobby findHobbyById(int id);

    public List<Person> getAllPerson();

    public List<City> getAllCity();

    public List<Hobby> getAllHobby();

    public List<Profession> getAllProfession();

    public void deletePerson(Person person);
    public void deleteCity(City city);
    public void deleteProfession(Profession profession);
    public void deleteHobby(Hobby hobby);

    public void deleteAllPerson();

    public void deleteAllCity();

    public void deleteAllProfession();

    public void deleteAllHobby();

    public  void  updatePerson(Person person);
    public  void  updateCity(City city);
    public  void  updateProfession(Profession profession);
    public  void  updateHobby(Hobby hobby);

    public  void  addAllPeople(Collection<Person> people);
    public  void  addAllCities(Collection<City> people) ;
    public  void  addAllHobbies(Collection<Hobby> people);
    public  void  addAllProfession(Collection<Profession> people) ;
}