package pl.sda.exercises;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@Data

@Entity
@Table(name = "person")
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int id;

    public Person() {
        hobbies = new LinkedList<>();
    }

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @ManyToOne(cascade =  CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "city_id")
    private  City city;

    public  void addCity(City city) {
        this.setCity(city);
        city.getPersonSet().add(this);
    }

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "profession_id")
    private  Profession profession;

    public  void  addProfession(Profession profession){
        this.setProfession(profession);
        profession.getPersonSet().add(this);
    }

    @ManyToMany(cascade = CascadeType.MERGE, fetch =FetchType.EAGER)
    @JoinTable(name = "person_hobby",
    joinColumns=@JoinColumn(name = "person_id"),
    inverseJoinColumns=@JoinColumn(name = "hobby_id"))
    private List<Hobby> hobbies;

    public  void  addHobby(Hobby hobby) {
        this.hobbies.add(hobby);
        hobby.getPeople().add(this);
    }
    public  void  deleteHobby(Hobby hobby) {
        hobbies.remove(hobby);
        hobby.getPeople().remove(this);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        return (this.getId()==((Person) o).getId() && this.getSurname().equals(((Person) o).getSurname()) && this.getName().equals(((Person) o).getName()));
    }

    @Override
    public int hashCode() {
        return 123;
    }

    @Override
    public String toString() {
        return surname+" "+name;
    }

    public String hobbiesPresentation() {
        String result = new String();
        for (Hobby hobby:hobbies) {
            result+=hobby.getName();
            result+=" , ";
        }


        return  result;
    }


}
