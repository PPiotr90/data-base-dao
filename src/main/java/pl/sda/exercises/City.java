package pl.sda.exercises;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
@Table(name = "city")
public class City  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int id;
    @Column(nullable = false)
    private String name;


    @OneToMany(mappedBy = "city", fetch = FetchType.EAGER)
    private Set<Person> personSet = new HashSet<>();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        return ((City) o).getId()== this.getId() && this.getName().equals(((City) o).getName());
    }

    @Override
    public int hashCode() {
       return 123;
    }

    @Override
    public String toString() {
        return name;
    }
}
